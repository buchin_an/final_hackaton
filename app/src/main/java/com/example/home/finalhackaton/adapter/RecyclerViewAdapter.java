package com.example.home.finalhackaton.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.home.finalhackaton.CityWeather;
import com.example.home.finalhackaton.R;
import com.example.home.finalhackaton.WeatherAPI;
import com.example.home.finalhackaton.WeatherDay;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<WeatherDay> weatherDays;
    private OnRecyclItemClic onRecyclItemClic;

    public RecyclerViewAdapter(Context context, List<WeatherDay> weathersDays, OnRecyclItemClic onRecyclItemClic) {
        this.context = context;
        this.weatherDays = weathersDays;
        this.onRecyclItemClic = onRecyclItemClic;
    }

    public interface OnRecyclItemClic {
        void onClick(int position);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherDay weatherDay = weatherDays.get(position);
        holder.temperature.setText(weatherDay.getTemp());
    }

    @Override
    public int getItemCount() {
        return weatherDays.size();
    }

    public WeatherDay getItem(int position) {
        return weatherDays.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView pogoda_info;
        TextView temperature;
        TextView feel;
        TextView pressure;

        public ViewHolder(View item) {
            super(item);
            icon =  item.findViewById(R.id.imageView);
            pogoda_info =  item.findViewById(R.id.pogoda_info);
            temperature = item.findViewById(R.id.temperature);
            feel =  item.findViewById(R.id.feel);
            pressure =  item.findViewById(R.id.pressure);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClic.onClick(getAdapterPosition());
                }
            });
        }
    }
}
