package com.example.home.finalhackaton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.home.finalhackaton.adapter.RecyclerViewAdapter;

public class FullInfo extends AppCompatActivity {
    ImageView icon;
    TextView pogodaInfo, temperature, feel, pressure, timeDawn, moonRise, moonset, chance_of_rain, chance_of_snow, probability_of_fog, wind_speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_info);

        int iconChnage;
        String pogodaInfoValue, temperatureValue, feelValue;


        temperatureValue = getIntent().getStringExtra("temperature");
        feelValue = getIntent().getStringExtra("feel");

        pogodaInfo = findViewById(R.id.pogoda_info);
        temperature = findViewById(R.id.temperature);
        feel =  findViewById(R.id.feel);
        pressure =  findViewById(R.id.pressure);
        timeDawn = findViewById(R.id.time_dawn);
        moonRise =  findViewById(R.id.moon_rise);
        moonset = findViewById(R.id.moonset);
        chance_of_rain = findViewById(R.id.chance_of_rain);
        chance_of_snow = findViewById(R.id.chance_of_snow);
        probability_of_fog = findViewById(R.id.probability_of_fog);
        wind_speed = findViewById(R.id.wind_speed);
        temperature.setText(temperatureValue);

    }
}
