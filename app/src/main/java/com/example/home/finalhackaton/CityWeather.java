package com.example.home.finalhackaton;

public class CityWeather {
    String description;
    int icon;
    String temp;
    String feelTemp;
    String clouds;
    String pressure;
    String humidity;
    String windSpeed;
    long sunrise;
    long sunset;


    public CityWeather(String description, int icon, String temp, String feelTemp, String clouds, String pressure, String humidity, String windSpeed, long sunrise, long sunset) {
        this.description = description;
        this.icon = icon;
        this.temp = temp;
        this.feelTemp = feelTemp;
        this.clouds = clouds;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }
}
