package com.example.home.finalhackaton.util;

import android.content.Context;
import android.content.SharedPreferences;

public class CheckUpdate {
    private static int lastUpdate;
    private static SharedPreferences updateTimeStorage;
    private static final String UPDATE_TIME = "update time";
    private static Context context;

    public CheckUpdate(Context context) {
        this.context = context;
    }

    public boolean needUpdate() {
        lastUpdate = loadUpdateTime();
        if (lastUpdate == 0) {
            return true;
        } else if (System.currentTimeMillis() - lastUpdate < 24 * 60 * 60 * 1000) {
            return true;
        }
        return false;
    }

    public static void saveUpdateTime(int time) {
        updateTimeStorage = context.getSharedPreferences(UPDATE_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = updateTimeStorage.edit().putInt(UPDATE_TIME, time);
        editor.apply();
    }

    public static int loadUpdateTime() {
        updateTimeStorage = context.getSharedPreferences(UPDATE_TIME, Context.MODE_PRIVATE);
        if (updateTimeStorage.contains(UPDATE_TIME)) {
            return updateTimeStorage.getInt(UPDATE_TIME, 0);
        }
        return 0;
    }
}
