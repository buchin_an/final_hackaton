package com.example.home.finalhackaton.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.home.finalhackaton.WeatherDay;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class WetherLocalStore {
    public final String WEATHER_STORAGE = "weather storage";
    private SharedPreferences weatherLocalStore;
    private List<WeatherDay> weatherList;
    private Context context;
    private static Type itemsListType = new TypeToken<List<WeatherDay>>() {
    }.getType();

    public WetherLocalStore(List<WeatherDay> weatherList, Context context) {
        this.weatherList = weatherList;
        this.context = context;
    }

    public void storeWeather() {
        weatherLocalStore = context.getSharedPreferences(WEATHER_STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = weatherLocalStore.edit()
                .putString(WEATHER_STORAGE, new Gson().toJson(weatherList));
        editor.apply();
    }

    public List<WeatherDay> loadWEather() {
        if (weatherLocalStore.contains(WEATHER_STORAGE)) {
            return new Gson().fromJson(weatherLocalStore.getString(WEATHER_STORAGE, ""), itemsListType);
        }
        return null;
    }
}
