package com.example.home.finalhackaton;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.example.home.finalhackaton.adapter.RecyclerViewAdapter;


import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.OnRecyclItemClic{
WeatherAPI.ApiInterface api;
double lat = 49.9903;
double lon = 36.2304;
String units = "metric";

    RecyclerViewAdapter adapter;
    Button button;
    boolean work = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        api = WeatherAPI.getClient().create(WeatherAPI.ApiInterface.class);
        Call<WeatherForecast> call = api.getForecast(lat, lon, units, WeatherAPI.KEY );
        call.enqueue(new retrofit2.Callback<WeatherForecast>() {
            @Override
            public void onResponse(Call<WeatherForecast> call, retrofit2.Response<WeatherForecast> response) {
                final RecyclerView listWeather = findViewById(R.id.recyclerView);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                listWeather.setLayoutManager(layoutManager);
                adapter = new RecyclerViewAdapter(MainActivity.this, response.body().getItems(), MainActivity.this);
                listWeather.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(int position) {
        Intent intent = new Intent(this, FullInfo.class);
        intent.putExtra("temperature", adapter.getItem(position).getTemp());
        startActivity(intent);
    }
}